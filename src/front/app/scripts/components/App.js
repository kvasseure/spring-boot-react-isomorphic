import React from 'react';
import {Route, Link} from 'react-router-dom';
import Home from 'components/Home';
import Child from 'components/Child';
import 'styles/main.css';

export default function App() {
  return (
    <div>
      <h1>Hello Server Side Rendering!!</h1>

      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/child">Child</Link></li>
      </ul>

      <Route exactly path="/" component={Home}/>
      <Route path="/child" component={Child}/>

    </div>
  );
}